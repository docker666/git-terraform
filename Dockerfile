FROM registry.gitlab.com/gitlab-org/terraform-images/branches/v0-43-2-1.0:b5234772b267a46193a65b3071e9bf2bbcbf192f
RUN apk --update add curl git jq wget git-lfs gpg less openssh && \
    git lfs install && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
